package mk.ukim.finki.emt.fss.model.events;

import lombok.Getter;
import mk.ukim.finki.emt.fss.model.Student;
import org.springframework.context.ApplicationEvent;

import java.time.LocalDateTime;

@Getter
public class StudentCreatedEvent extends ApplicationEvent {

    private LocalDateTime when;

    public StudentCreatedEvent(Student source) {
        super(source);
        this.when = LocalDateTime.now();
    }

    public StudentCreatedEvent(Student source, LocalDateTime when) {
        super(source);
        this.when = when;
    }
}
