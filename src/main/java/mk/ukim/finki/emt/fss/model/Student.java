package mk.ukim.finki.emt.fss.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name="student")
public class Student  {

    @Id
    @Column(name="id")
    private String index;

    @Version
    private Long version;

    private String name;

    @ManyToOne
    private StudyProgram studyProgram;

    @ManyToMany
    private List<Course> courses;

}
