package mk.ukim.finki.emt.fss.jobs;

import mk.ukim.finki.emt.fss.service.StudentService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

    private StudentService studentService;

    public ScheduledTasks(StudentService studentService) {
        this.studentService = studentService;
    }

//    @Scheduled(cron="0 0 12 * * ?")
//    @Scheduled(fixedRate = 5000)
    public void refreshMViews() {
        studentService.refreshMV();
    }
}
