package mk.ukim.finki.emt.fss.model.projections;

public interface StudentProjection {

    String getIndex();
    String getName();
}
