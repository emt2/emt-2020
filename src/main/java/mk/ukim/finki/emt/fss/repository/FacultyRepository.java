package mk.ukim.finki.emt.fss.repository;

import mk.ukim.finki.emt.fss.model.Faculty;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FacultyRepository extends JpaRepository<Faculty,Long> {
}
