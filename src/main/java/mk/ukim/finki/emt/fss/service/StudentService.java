package mk.ukim.finki.emt.fss.service;

import mk.ukim.finki.emt.fss.model.Student;

import java.util.List;

public interface StudentService {
    List<Student> getAllStudents();
    Student createNewStudent(Student student);
    void refreshMV();
}
